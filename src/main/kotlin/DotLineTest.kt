import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking {
        launch {
            var count = 0
            while (count < 100) {
                print(StringBuilder().append("."))
                delay(300)
                count++
            }
        }
    }
}