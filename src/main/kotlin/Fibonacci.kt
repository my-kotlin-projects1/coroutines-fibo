import kotlinx.coroutines.*
import java.math.BigInteger
import kotlin.coroutines.CoroutineContext

fun main() = runBlocking {
    println("My program STARTS on: ${Thread.currentThread().name}")
    var job3: Job? = null

    val job1 = launch {
        doCoroutine(1, 50)
    }

    val job2 = launch {
        doCoroutine(2, 100)
    }

    try {
        withTimeout(1000) {
            job3 = launch {
                doCoroutine(3, 150)
            }

            job1.join()
            job2.join()
            job3?.join()
        }
    } catch (e: TimeoutCancellationException) {
        println("Executing takes too much time, program stops")
    }

    launch {
        while (job1.isActive || job2.isActive || job3?.isActive == true) {
                val line = StringBuilder()
                line.append(".")
                print(line)
                delay(300)
            }
    }

    println("My program ENDS on: ${Thread.currentThread().name}")
}

suspend fun doCoroutine(coroutineNumber: Int, index: Int) {
    println("Coroutine$coroutineNumber STARTS: running in thread ${Thread.currentThread().name}")
    val result = Fibonacci.take(index)
    println("Coroutine$coroutineNumber ENDS: Fibo $index: $result")
}

object Fibonacci {
    suspend fun take(index: Int): BigInteger {
        var number1 = BigInteger("0")
        var number2: BigInteger = BigInteger("1")
        for (i in 2..index) {
            delay(200)
            // println("calculating Fibo $index on ${Thread.currentThread().name}")
            val sum = number1 + number2
            number1 = number2
            number2 = sum
        }
        return number2
    }
}